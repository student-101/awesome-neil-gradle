import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.webage.GreetingService;

public class GreetingTest {
	
	@Test
	public void TestGreeting() {
		GreetingService GSObj = new GreetingService();
		String name = "Neil";
		String retGreeting = GSObj.getGreeting(name);
		assertEquals("Hello - " + name, retGreeting);
	}

}
